package com.example.timetabletask.service;


public interface TimetableService {

    void executeTimestampWithoutParams();
    void executeTimestampWithParam();

}