package com.example.timetabletask.service;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.example.timetabletask.model.MyTimetable;
import com.example.timetabletask.repository.TimetableRepository;
import com.example.timetabletask.ResourceNotFoundException;

@Service
public class TimetableServiceImpl implements TimetableService {

    @Autowired
    private TimetableRepository timetableRepository;

    @Autowired
    private ApplicationContext context;

    private Logger loggerFactory = LoggerFactory.getLogger(this.getClass().getPackage().getName());

    private final BlockingQueue<LocalTime> localTimes = new LinkedBlockingQueue<>(3600);

    @Override
    public void executeTimestampWithoutParams() {

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {

            final LocalTime nowTime = LocalTime.now();
            try {
                localTimes.add(nowTime);
            } catch (Throwable e) {
                loggerFactory.error(e.getMessage());
            }
            loggerFactory.info("LOG: Timestamp " + nowTime + " added to the queue for processing... " + localTimes.size());

        }, 5L, 1L, TimeUnit.SECONDS);

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            if (!localTimes.isEmpty()) {
                try {
                    loggerFactory.info("LOG: Object " + localTimes.peek() + " saved to DB.");
                    timetableRepository.save(MyTimetable.builder().timestamp(localTimes.peek()).build());
                    localTimes.poll();
                } catch (Throwable e) {
                    loggerFactory.error("LOG: Connection with DB lost.");
                }
            }
        }, 10000L, 200L, TimeUnit.MILLISECONDS);
    }


    @Override
    public void executeTimestampWithParam() {

        List<MyTimetable> dataFromDB = Optional.ofNullable(timetableRepository.findAll()).orElseThrow(() -> new ResourceNotFoundException("There is no data in DB"));

        loggerFactory.info("LOG: ALL records from DataBase are following:");
        for (MyTimetable data : dataFromDB) {
            loggerFactory.info(" " + data.getTimestamp());
        }
        loggerFactory.info("LOG: Closing Application...");
        close();
    }

    private void close() {
        SpringApplication.exit(context, () -> 0);
    }

}