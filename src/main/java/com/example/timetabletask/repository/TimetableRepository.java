package com.example.timetabletask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.timetabletask.model.MyTimetable;

@Repository
public interface TimetableRepository extends JpaRepository<MyTimetable, Long> {
}