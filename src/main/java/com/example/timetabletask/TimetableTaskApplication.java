package com.example.timetabletask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.timetabletask.service.TimetableService;

@SpringBootApplication
public class TimetableTaskApplication implements CommandLineRunner {

    private static Logger loggerFactory = LoggerFactory.getLogger(Class.class.getPackage().getName());

    @Autowired
    private TimetableService timetableService;

    public static void main(String[] args) {

        SpringApplication.run(TimetableTaskApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        if (args.length > 0 && args[0].equals("-p")) {
            loggerFactory.info("LOG: Running TimetableApplication with args: " + args[0]);
            timetableService.executeTimestampWithParam();
        } else {
            loggerFactory.info("LOG: Running TimetableApplication without args");
            timetableService.executeTimestampWithoutParams();
        }
    }

}